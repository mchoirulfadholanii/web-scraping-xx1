const { chromium } = require("playwright");
const fs = require("fs").promises; // Modul fs untuk menulis ke file

(async () => {
  // Membuka browser Chromium
  const browser = await chromium.launch({
    headless: false,
    args: [
      "--no-sandbox", // Diperlukan untuk Linux
      "--disable-devtools", // Menonaktifkan DevTools
      "--disable-extensions", // Menonaktifkan ekstensi
      "--disable-popup-blocking", // Menonaktifkan pemblokiran pop-up
    ],
  });

  // Membuka halaman web yang ingin di-scraper
  const context = await browser.newContext({
    userAgent:
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64)" +
      " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
  });
  const page = await context.newPage();
  await page.goto("https://m.21cineplex.com/", { timeout: 60000 });

  await page.getByRole("link", { name: "M-Tix" }).click();
  await page.getByRole("link", { name: " Login" }).click();
  await page.getByPlaceholder("Handphone Number").click();
  await page.getByPlaceholder("Handphone Number").fill("085785199333");
  await page.waitForTimeout(2000);
  await page.getByPlaceholder("PIN/Password").click();
  await page.getByPlaceholder("PIN/Password").fill("585242");
  await page.waitForTimeout(2000);
  await page.getByRole("button", { name: "Login " }).click();
  await page.waitForTimeout(2000);
  await page.getByPlaceholder("OTP Code").click();
  await page.getByPlaceholder("OTP Code").fill("406148");
  await page.getByRole("button", { name: "CONTINUE" }).click();
  await page.waitForTimeout(2000);
  await page.getByRole("link", { name: "Playing in - JAKARTA " }).click();
  await page.waitForTimeout(2000);

  const kota = "AMBON";
  const teater = {
    cinemaName: "INDO XX!",
  };

  await page.getByText(kota).click();

  await page.getByText("AMBON CITY CENTER XXI").click();

  // baris awal get atribut film

  // Temukan elemen <a> di dalam elemen <li>
  const movieElements = await page.$$("li.list-group-item");

  const films = await Promise.all(
    movieElements.map(async (movieElement) => {
      const title = await page.evaluate((element) => {
        const titleElement = element.querySelector("a:not([href])");
        return titleElement ? titleElement.textContent.trim() : null;
      }, movieElement);

      const duration = await movieElement.$eval(".glyphicon-time", (span) =>
        span.nextSibling.textContent.trim()
      );

      // Ambil tanggal dari elemen dengan class 'p_date'
      const date = await movieElement.$eval(".p_date:not(:empty)", (pDate) =>
        pDate.innerText.trim()
      );

      // Ambil waktu tayang dari elemen dengan class 'div_schedule'
      const scheduleTimes = await movieElement.$$eval(
        ".div_schedule",
        (scheduleElements) =>
          scheduleElements.map((schedule) => schedule.innerText.trim())
      );

      return { title, duration, date, scheduleTimes };
    })
  );

  const seatInfoMap = new Map();

  // baris akhir get atribut film
  // Iterate through each film
  for (const film of films) {
    const filmKeyBase = `Film: ${film.title} - Tanggal: ${film.date} - Kota: ${kota} - ${teater.cinemaName}`;

    // Iterate through each time for the current film
    for (const time of film.scheduleTimes) {
      console.log(`  Waktu: ${time}`);

      // Klik waktu
      await page.click(`p.p_time a.div_schedule[onclick*="time_show=${time}"]`);

      // Tunggu beberapa detik
      await page.waitForTimeout(2000);

      // Klik tombol 'CONTINUE'
      await page.getByRole("button", { name: "CONTINUE" }).click();

      // Tunggu beberapa detik setelah mengklik 'CONTINUE'
      await page.waitForTimeout(2000);

      // Mendapatkan jumlah gambar kursi gratis
      const seatImages = await page.$$('img[src="images/seat_free.png"]');
      const seatCount = seatImages.length - 1;

      // Buat kunci film untuk setiap waktu
      const filmKey = `${filmKeyBase} - Waktu: ${time}`;

      // Simpan informasi kursi ke dalam seatInfoMap
      seatInfoMap.set(filmKey, { time, seatCount });

      // Kembali ke halaman sebelumnya
      await page.goBack();

      // Tunggu beberapa detik lagi sebelum melanjutkan ke waktu berikutnya
      await page.waitForTimeout(2000);
    }
  }

  //simpan data akhir
  // Menyimpan informasi kursi ke dalam file txt
  const seatInfoArray = Array.from(seatInfoMap.entries()).map(
    ([key, value]) => `${key} - Kursi Tersedia: ${value.seatCount}`
  );
  const seatInfoText = seatInfoArray.join("\n");

  try {
    await fs.writeFile("informasi_kursi.txt", seatInfoText);
    console.log(
      "Informasi kursi telah disimpan ke dalam file informasi_kursi.txt"
    );
  } catch (err) {
    console.error("Gagal menyimpan informasi kursi ke dalam file:", err);
  }

  await browser.close();
})();
