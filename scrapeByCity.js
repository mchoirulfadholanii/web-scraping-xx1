const { chromium } = require("playwright");
const fs = require("fs").promises;
const createCsvWriter = require("csv-writer").createObjectCsvWriter;

async function launchBrowser() {
  return await chromium.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-devtools",
      "--disable-extensions",
      "--disable-popup-blocking",
    ],
  });
}

async function extractTheaterInfo(page) {
  const theaterElements = await page.$$("li.list-group-item");

  const theaters = await Promise.all(
    theaterElements.map(async (theaterElement) => {
      const cinemaName = await theaterElement.$eval('div[id="id"]', (div) =>
        div.textContent.trim()
      );

      const cinemaIdMatch = /cinema_id=([^&]+)/.exec(
        await theaterElement.getAttribute("onclick")
      );
      const cinemaId = cinemaIdMatch ? cinemaIdMatch[1] : null;

      return { cinemaName, cinemaId };
    })
  );

  console.log("theater", theaters);
  return theaters;
}

async function extractFilms(page) {
  const movieElements = await page.$$("li.list-group-item");
  return await Promise.all(
    movieElements.map(async (movieElement) => {
      const title = await page.evaluate((element) => {
        const titleElement = element.querySelector("a:not([href])");
        return titleElement ? titleElement.textContent.trim() : null;
      }, movieElement);

      const date = await movieElement.$eval(".p_date:not(:empty)", (pDate) =>
        pDate.innerText.trim()
      );

      const scheduleTimes = await movieElement.$$eval(
        ".div_schedule",
        (scheduleElements) => {
          return scheduleElements
            .filter(
              (scheduleElement) =>
                !scheduleElement.classList.contains("disabled")
            )
            .map((scheduleElement) => scheduleElement.innerText.trim());
        }
      );

      return {
        title,
        date,
        scheduleTimes,
      };
    })
  );
}

async function processTimeSlot(page, time) {
  await page.click(`p.p_time a.div_schedule[onclick*="time_show=${time}"]`);
  await page.waitForTimeout(2000);
  await page.getByRole("button", { name: "CONTINUE" }).click();
  await page.waitForTimeout(2000);
}

async function main(city, username, password, otp) {
  const browser = await launchBrowser();
  const context = await browser.newContext({
    userAgent:
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64)" +
      " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
  });
  const page = await context.newPage();

  // Langkah-langkah login
  await page.goto("https://m.21cineplex.com/", { timeout: 40000 });
  await page.getByRole("link", { name: "M-Tix" }).click();
  await page.getByRole("link", { name: " Login" }).click();
  await page.getByPlaceholder("Handphone Number").click();
  await page.getByPlaceholder("Handphone Number").fill(username);
  await page.waitForTimeout(2000);
  await page.getByPlaceholder("PIN/Password").click();
  await page.getByPlaceholder("PIN/Password").fill(password);
  await page.waitForTimeout(2000);
  await page.getByRole("button", { name: "Login " }).click();
  await page.waitForTimeout(2000);
  await page.getByPlaceholder("OTP Code").click();
  await page.getByPlaceholder("OTP Code").fill(otp);
  await page.getByRole("button", { name: "CONTINUE" }).click();
  await page.waitForTimeout(2000);
  await page.getByRole("link", { name: "Playing in - JAKARTA " }).click();
  await page.waitForTimeout(2000);

  const kota = city;

  await page.getByText(kota).click();
  await page.waitForTimeout(2000);
  const theaterSelectionPageUrl = page.url();

  const theaters = await extractTheaterInfo(page);

  await page.waitForTimeout(2000);

  const currentDate = new Date();
  const formattedDate = currentDate.toISOString().replace(/:/g, "-");

  const csvWriter = createCsvWriter({
    path: `jadwal_film_${kota}_${formattedDate}.csv`, // Nama file berdasarkan timestamp
    header: [
      { id: "tanggal", title: "Tanggal" },
      { id: "judul_film", title: "Judul Film" },
      { id: "kota", title: "Kota" },
      { id: "nama_theater", title: "Theater" },
      { id: "waktu", title: "Waktu tayang" },
      { id: "seat_count", title: "Jumlah Penonton" },
    ],
  });

  let csvData = [];

  for (const theater of theaters) {
    await page.getByText(theater.cinemaName).click();
    await page.waitForTimeout(2000);

    const films = await extractFilms(page);
    await page.waitForTimeout(2000);

    for (const film of films) {
      for (const time of film.scheduleTimes) {
        // Menghitung waktu untuk membandingkan dengan waktu saat ini
        const [hours, minutes] = time.split(":").map(Number);
        const filmTime = new Date();
        filmTime.setHours(hours);
        filmTime.setMinutes(minutes);

        const timeDiff = filmTime - currentDate;

        // Jika film akan berjalan dalam 1 jam ke depan
        if (timeDiff > 0 && timeDiff <= 60 * 60 * 1000) {
          await processTimeSlot(page, time);

          const seatImages = await page.$$('img[src="images/seat_booked.png"]');
          const seatCount = seatImages.length - 1;

          const jadwal = {
            tanggal: film.date,
            judul_film: film.title,
            kota: kota,
            nama_theater: theater.cinemaName,
            waktu: time,
            seat_count: seatCount,
          };

          // Menambahkan data jadwal ke array csvData
          csvData.push(jadwal);

          await page.goBack();
          await page.waitForTimeout(2000);
        }
      }
    }

    await page.goto(theaterSelectionPageUrl);
    await page.waitForTimeout(2000);
  }

  // Menulis data ke file CSV
  if (csvData.length > 0) {
    await csvWriter.writeRecords(csvData);
  } else {
    // Jika tidak ada data yang ditemukan, tulis header saja
    await csvWriter.writeRecords([]);
  }

  await browser.close();
}

const args = process.argv.slice(2);
const [city, username, password, otp] = args;
main(city, username, password, otp);
