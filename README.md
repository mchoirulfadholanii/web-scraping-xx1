# Web Scraping XX1 Microsoft Playwright

## Requirement

node.js versi 19 atau terbaru

## Install

```
npm install playwright
```

## Menjalankan Web Scrapping

```
node [namafile.js]
```

contoh:
```
node scrapeByTheaters.js
```

## Keterangan

Fitur yang tersedia pada project ini sebagai berikut:

**scrapeByFilms.js**: contoh scrape data dengan kota dan theater yang ditentukan di kode

**scrapeByFilms.js**: contoh scrape data dengan melakukan iterasi setiap theater dan setiap film

**scrapeByParams.js**: contoh scrape data dengan melakukan iterasi setiap theater dan setiap film menggunakan parameter KOTA USERNAME PASSWORD dan OTP

**scrapeByCity.js**: contoh scrape data dengan melakukan iterasi setiap theater dan setiap film menggunakan parameter KOTA USERNAME PASSWORD dan OTP dan menyimpan hasilnya ke file .CSV


input parameters untuk file scrapeByParams.js dan scrapeByCity.js

```
node scrapeByCity.js "NAMA KOTA" "NOMOR TELEPON" "PIN" "OTP"
```

contoh:

```node scrapeByCity.js MALANG 08578513433 584542 432546```



## Changelog
Versi 1.0
- pada versi 1 data login dan OTP tersimpan dalam script, harus disesuaikan dengan akun yang digunakan.

Versi 1.1
- menggunakan parameter kota username, password dan OTP pada saat menjalankan scrape data

Versi 1.2 
- ambil data penonton (scrapeByCity.js) dengan tetap menggunakan parameter kota username, password dan OTP dan menyimpan hasilnya ke file .CSV