const { chromium } = require("playwright");
const fs = require("fs").promises;

async function launchBrowser() {
  return await chromium.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-devtools",
      "--disable-extensions",
      "--disable-popup-blocking",
    ],
  });
}

async function extractTheaterInfo(page) {
  const theaterElements = await page.$$("li.list-group-item");

  const theaters = await Promise.all(
    theaterElements.map(async (theaterElement) => {
      const cinemaName = await theaterElement.$eval('div[id="id"]', (div) =>
        div.textContent.trim()
      );

      const cinemaIdMatch = /cinema_id=([^&]+)/.exec(
        await theaterElement.getAttribute("onclick")
      );
      const cinemaId = cinemaIdMatch ? cinemaIdMatch[1] : null;

      return { cinemaName, cinemaId };
    })
  );

  console.log("theater", theaters);
  return theaters;
}

async function extractFilms(page) {
  const movieElements = await page.$$("li.list-group-item");
  return await Promise.all(
    movieElements.map(async (movieElement) => {
      const title = await page.evaluate((element) => {
        const titleElement = element.querySelector("a:not([href])");
        return titleElement ? titleElement.textContent.trim() : null;
      }, movieElement);

      const date = await movieElement.$eval(".p_date:not(:empty)", (pDate) =>
        pDate.innerText.trim()
      );

      const scheduleTimes = await movieElement.$$eval(
        ".div_schedule",
        (scheduleElements) => {
          return scheduleElements
            .filter(
              (scheduleElement) =>
                !scheduleElement.classList.contains("disabled")
            )
            .map((scheduleElement) => scheduleElement.innerText.trim());
        }
      );

      return {
        title,
        date,
        scheduleTimes,
      };
    })
  );
}

async function processTimeSlot(page, time) {
  await page.click(`p.p_time a.div_schedule[onclick*="time_show=${time}"]`);
  await page.waitForTimeout(2000);
  await page.getByRole("button", { name: "CONTINUE" }).click();
  await page.waitForTimeout(2000);
}

async function saveSeatInfoToFile(seatInfoMap) {
  const seatInfoArray = Array.from(seatInfoMap.entries()).map(
    ([key, value]) => `${key} - Kursi Tersedia: ${value.seatCount}`
  );

  const seatInfoText = seatInfoArray.join("\n");

  try {
    await fs.writeFile("informasi_kursi.txt", seatInfoText);
    console.log(
      "Informasi kursi telah disimpan ke dalam file informasi_kursi.txt"
    );
  } catch (err) {
    console.error("Gagal menyimpan informasi kursi ke dalam file:", err);
  }
}

async function performLogin(page, username, password, otp) {
  await page.goto("https://m.21cineplex.com/", { timeout: 60000 });

  await page.getByRole("link", { name: "M-Tix" }).click();
  await page.getByRole("link", { name: " Login" }).click();
  await page.getByPlaceholder("Handphone Number").click();
  await page.getByPlaceholder("Handphone Number").fill(username);
  await page.waitForTimeout(2000);
  await page.getByPlaceholder("PIN/Password").click();
  await page.getByPlaceholder("PIN/Password").fill(password);
  await page.waitForTimeout(2000);
  await page.getByRole("button", { name: "Login " }).click();
  await page.waitForTimeout(2000);
  await page.getByPlaceholder("OTP Code").click();
  await page.getByPlaceholder("OTP Code").fill(otp);
  await page.getByRole("button", { name: "CONTINUE" }).click();
  await page.waitForTimeout(2000);
  await page.getByRole("link", { name: "Playing in - JAKARTA " }).click();
  await page.waitForTimeout(2000);
}

async function main(city, username, password, otp) {
  const browser = await launchBrowser();
  const context = await browser.newContext({
    userAgent:
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64)" +
      " AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
  });
  const page = await context.newPage();

  await performLogin(page, username, password, otp);

  const kota = city;

  await page.getByText(kota).click();
  await page.waitForTimeout(6000);
  const theaterSelectionPageUrl = page.url();

  const theaters = await extractTheaterInfo(page);

  await page.waitForTimeout(6000);

  const seatInfoMap = new Map();

  for (const theater of theaters) {
    console.log(
      `Memproses theater: ${theater.cinemaName} (${theater.cinemaId})`
    );

    await page.getByText(theater.cinemaName).click();
    await page.waitForTimeout(6000);

    const films = await extractFilms(page);
    await page.waitForTimeout(6000);

    const filmSplit = [films[0]];

    for (const film of filmSplit) {
      const filmKeyBase = `Film: ${film.title} - Tanggal: ${film.date} - Kota: ${kota} - ${theater.cinemaName}`;

      for (const time of film.scheduleTimes) {
        console.log(`  Waktu: ${time}`);

        await processTimeSlot(page, time);

        const seatImages = await page.$$('img[src="images/seat_free.png"]');
        const seatCount = seatImages.length - 1;

        const filmKey = `${filmKeyBase} - Waktu: ${time}`;
        seatInfoMap.set(filmKey, { time, seatCount });

        await page.goBack();
        await page.waitForTimeout(2000);
      }
    }

    await page.goto(theaterSelectionPageUrl);
    await page.waitForTimeout(2000);
  }

  await saveSeatInfoToFile(seatInfoMap);
  await browser.close();
}

const args = process.argv.slice(2);
const [city, username, password, otp] = args;
main(city, username, password, otp);
